package com.raghu.springsecurity.entity;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;


@Entity
@Table(name="authorities")

public class Authorities   {
	
	




	 
	 
	 
	public Authorities() {	} 
	 
	public Authorities( String authority) {
		
		this.authority = authority;
	}




	 @OneToOne
	 @PrimaryKeyJoinColumn
	 private User user;
	
	 public void setUser(User user) {
			this.user = user;
		}
	 


	@Id
	@Column(name="username")
	private String username;
	
	public String getUser() {
		return username;
	}


	public void setUser(String user) {
		this.username = user;
	}



	@Column(name="authority")
	private String authority;

	public String getAuthority() {
		return authority;
	}

	public void setAuthority(String authority) {
		this.authority = authority;
	}





	




	
	
}
