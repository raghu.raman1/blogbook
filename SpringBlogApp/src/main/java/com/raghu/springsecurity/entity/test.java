package com.raghu.springsecurity.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "test")
public class test {

	@Column(name="username")
	private String username;
	
	
	public test(String username, String blog) {
		super();
		this.username = username;
		this.blog = blog;
	}


	public test() {
		super();
	}

@Id
	@Column(name="blog")
	private String blog;


	public String getUsername() {
		return username;
	}


	public void setUsername(String username) {
		this.username = username;
	}


	public String getBlog() {
		return blog;
	}


	public void setBlog(String blog) {
		this.blog = blog;
	}
}
