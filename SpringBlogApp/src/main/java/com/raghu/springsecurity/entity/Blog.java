package com.raghu.springsecurity.entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

@Entity
@Table(name="blog")
public class Blog {
	
	public Blog() {}
	

	public Blog( String blog_content, Integer like, Integer comments) {
	
	
		this.blog_content = blog_content;
		this.likes = likes;
		this.comments = comments;
	}


	 @Id
	 @GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer blog_id;

	public Integer getBlog_id() {
		return blog_id;
	}

	public void setBlog_id(Integer blog_id) {
		this.blog_id = blog_id;
	}
	
	
	
	 
    @OneToMany(mappedBy = "blog",  cascade = {CascadeType.REMOVE})
    private List<Likes> like;

	public List<Likes> getLike() {
		return like;
	}

	public void setLike(List<Likes> like) {
		this.like = like;
	}

	

	 @OneToMany(mappedBy = "blogForComment", cascade = {CascadeType.REMOVE})
	  private List<Comments> comment;

	public List<Comments> getComment() {
		return comment;
	}


	public void setComment(List<Comments> comment) {
		this.comment = comment;
	}


	@ManyToOne
	 @JoinColumn(name = "username")
	 private User username;
	
	public User getUsername() {
		return username;
	}

	public void setUsername(User username) {
		this.username = username;
	}
	

	
	@Column(name="blog")
	private String blog_content;

	public String getBlog_content() {
		return blog_content;
	}
	public void setBlog_content(String blog_content) {
		this.blog_content = blog_content;
	}

	
	@Column(name="likes")
	private Integer likes;
	public Integer getLikes() {
		return likes;
	}

	public void setLikes(Integer likes) {
		this.likes = likes;
	}

	

	
	@Column(name="comments")
	private Integer comments;
	
	public Integer getComments() {
		return comments;
	}

	public void setComments(Integer comments) {
		this.comments = comments;
	}





	

	
	
}
