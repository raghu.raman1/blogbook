package com.raghu.springsecurity.DAO;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.raghu.springsecurity.entity.Authorities;
import com.raghu.springsecurity.entity.Blog;
import com.raghu.springsecurity.entity.Comments;
import com.raghu.springsecurity.entity.Friends;
import com.raghu.springsecurity.entity.Likes;
import com.raghu.springsecurity.entity.User;

@Service
public class CommentDAO {

	@Autowired
	private   Session_factory sessionFactory;
	 
		public  Boolean addcomment(Integer blog_id, String username, String comment) {
			// TODO Auto-generated method stub
			Session session =	sessionFactory.getCurrentSession();
			     
			        session.beginTransaction();
			       
			      
			        Comments thecomment= new Comments(username ,comment);

			        List<Comments> allComments = new ArrayList<Comments>();
	        
			        Blog theblog = (Blog) session.get(Blog.class, blog_id);
		   
			        thecomment.setBlogForComment(theblog);
			        
			        allComments.add(thecomment);
			        
			        theblog.setComment(allComments);
			        
			        
	
			      
		        session.save(thecomment);
			        session.getTransaction().commit();
			     
			        return true ;
		}

	
	
	
	
}
