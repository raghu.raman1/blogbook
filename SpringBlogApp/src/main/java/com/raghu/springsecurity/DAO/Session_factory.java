package com.raghu.springsecurity.DAO;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.springframework.stereotype.Service;

import com.raghu.springsecurity.entity.Authorities;
import com.raghu.springsecurity.entity.Blog;
import com.raghu.springsecurity.entity.Comments;
import com.raghu.springsecurity.entity.Friends;
import com.raghu.springsecurity.entity.Likes;
import com.raghu.springsecurity.entity.User;
import com.raghu.springsecurity.entity.test;

@Service
public class Session_factory {

	
	  Session   getCurrentSession(){
		SessionFactory factory = new Configuration()
				.configure("hibernate.cfg.xml")
				.addAnnotatedClass(Blog.class)
				.addAnnotatedClass(User.class)
				.addAnnotatedClass(Authorities.class)
				.addAnnotatedClass(Likes.class)
				.addAnnotatedClass(Comments.class)
				.addAnnotatedClass(Friends.class)
				.addAnnotatedClass(test.class)
				.buildSessionFactory();
		
	return factory.getCurrentSession();
		
	}
	
}
