package com.raghu.springsecurity.DAO;

import org.hibernate.Session;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import com.raghu.springsecurity.entity.Authorities;
import com.raghu.springsecurity.entity.Blog;
import com.raghu.springsecurity.entity.Comments;
import com.raghu.springsecurity.entity.Friends;
import com.raghu.springsecurity.entity.Likes;
import com.raghu.springsecurity.entity.User;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;

@Service
public class LikeDAO {

	private EntityManager em; 

	
	private Likes likes;
	
	@Autowired
	private   Session_factory sessionFactory;
	
	 
	public  Boolean addlike(Integer blogid, String username) {
		// TODO Auto-generated method stub
		Session session =	sessionFactory.getCurrentSession();
		     
		        session.beginTransaction();
		        
		        Likes thelike = new Likes(username);

		        List<Likes> allLikes = new ArrayList<Likes>();
//		        
//		        
		        Blog theblog = (Blog) session.get(Blog.class, blogid);
	   
		        thelike.setBlog(theblog);
		        
		        allLikes.add(thelike);
		        
		        theblog.setLike(allLikes);
		        
		        
		        
		     //   System.out.println(like.getCompositeLikes());
	
		      
	        session.save(thelike);
		        session.getTransaction().commit();
		        
		        return true ;
	}


}
