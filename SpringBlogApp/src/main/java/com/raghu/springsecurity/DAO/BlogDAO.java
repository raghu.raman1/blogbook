package com.raghu.springsecurity.DAO;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.raghu.springsecurity.entity.Authorities;
import com.raghu.springsecurity.entity.Blog;
import com.raghu.springsecurity.entity.Comments;
import com.raghu.springsecurity.entity.Friends;
import com.raghu.springsecurity.entity.Likes;
import com.raghu.springsecurity.entity.Likes;
import com.raghu.springsecurity.entity.User;

@Service
public class BlogDAO {
	
	private User user;

	@Autowired
	private   Session_factory sessionFactory;
	
	//-----------------------------------------------------------------------------------------------
	//-----------------------------------------------------------------------------------------------
	//-----------------------------------------------------------------------------------------------

	public boolean addblog(String blog, String username) {

		Session session =	sessionFactory.getCurrentSession();
	
try {		


	Integer likes = 0;
	Integer comments = 0;
	
	session.beginTransaction();
	
	
	Blog newblog = new Blog( blog , 0 , 0);
	

     User theuser = (User) session.get(User.class, username);
     
     newblog.setUsername(theuser);
     

session.save(newblog);
session.getTransaction().commit();

return true ;

}
finally {
	session.close();
}

}

	
	//-----------------------------------------------------------------------------------------------
	//-----------------------------------------------------------------------------------------------
	//-----------------------------------------------------------------------------------------------

	public boolean deleteblog(Integer blog_id) {
		
		
		
			
		Session session =	sessionFactory.getCurrentSession();
		
System.out.println("CAMe after session get");
try {		


	session.beginTransaction();

     Blog theblog = (Blog) session.get(Blog.class, blog_id);
     
   
     


System.out.println("Came to Try in dao AFTER OBJ CREATION )" + "--");
	
session.delete(theblog);

session.getTransaction().commit();

return true ;

}
finally {
session.close();
}

}

	//-----------------------------------------------------------------------------------------------
	//--------------------model.add---------------------------------------------------------------------------
	//-----------------------------------------------------------------------------------------------

	
//	
//	
//	public List<Blog> getUserBlogs(String username) {
//		
//		System.out.println("CAMe before session addblog ");
//		
//			
//		Session session =	sessionFactory.getCurrentSession();
//		
//         System.out.println("CAMe after session get");
//try {		
//
//
//	System.out.println("Came to Try in test dao)");
//	
//
// 
//
//	session.beginTransaction();
//	
//	Query q=session.createQuery(" from Blog where username=:n "); 
//	
//	q.setParameter("n",username);   
//	
//	//q.executeUpdate();
//	
//	//List blogs=q.list();   
//	
//	
//    // Blog theblog = (Blog) session.get(Blog.class, username);
//     
//   
//     
//
//
//System.out.println("Came to Try in dao AFTER OBJ CREATION )" + "--");
//	
//
//session.getTransaction().commit();
////System.out.println("Blog GOT !" + blogs.toString());
////return blogs ;
//
//}
//finally {
//
//}
//
//}

	

	
	
	
	
	
	
	
	
}
