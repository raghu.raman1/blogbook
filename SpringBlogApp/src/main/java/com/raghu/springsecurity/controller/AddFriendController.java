package com.raghu.springsecurity.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.raghu.springsecurity.DAO.CommentDAO;
import com.raghu.springsecurity.DAO.FriendDAO;

@Controller
public class AddFriendController {
	
	@Autowired
	private FriendDAO friendDAO;
	
	
	
	 @RequestMapping("/home/search/addfriend")
	 protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		 try {

			 String username = request.getParameter("username");
			 String frienduser = request.getParameter("frienduser");
	
			 
			System.out.println("before calling service");
			friendDAO.addFriend(username , frienduser);


			
			}		
			catch (Exception e) {
				e.printStackTrace();
			}
		}
}
