package com.raghu.springsecurity.controller;

import java.io.IOException;
import java.net.http.HttpClient.Redirect;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.util.WebUtils;



@Controller
public class AppRouteController  {

	@GetMapping("/home")
	public String welcomepage( ) {
	
		    System.out.println("came from hoome");
			return "home";
	}
	
	@GetMapping("/")
	public String initpage() {
		return "redirect:home";	
	}


	
	@GetMapping("/login")
	public String showMyLoginPage(HttpServletRequest request ,HttpServletResponse res ) throws ServletException, IOException {
	
		
	return "login";
	}
	
	
	
	@GetMapping("/register")
	public String showMyRegisterPage(HttpServletRequest request ,HttpServletResponse res ) throws ServletException, IOException {
	return "register";
	}
	
	
	
	
	
	
	@GetMapping("/home/profile")
	public String showProfilep(HttpServletRequest request ,Model model) {	
		return "profile";
	}
	
	
	
	@GetMapping("/home/profile/addblog")
	public String showaddblog(HttpServletRequest request ,Model model) {	

		return "addblog";
	}
	

	
//	// add request mapping for /leaders
//
//	@GetMapping("/leaders")
//	public String showLeaders() {
//		
//		return "leaders";
//	}
	
//	// add request mapping for /systems
//	
//	@GetMapping("/systems")
//	public String showSystems() {
//		
//		return "systems";
//	}
//	@GetMapping("/access-denied")
//	public String showAccessDenied() {
//		return "access-denied";
//		
//	}
}










