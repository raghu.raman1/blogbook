package com.raghu.springsecurity.controller;

import javax.servlet.http.HttpServlet;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.stereotype.Controller;

import com.raghu.springsecurity.DAO.BlogDAO;
import com.raghu.springsecurity.DAO.RegisterDAO;

import java.io.IOException;

import javax.servlet.ServletException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.bind.annotation.RequestMapping;



@Controller
public class DeleteUserController {

	


		
	    @Autowired 
		private RegisterDAO registerdao;

		 @RequestMapping("/home/profile/deluser")
		 protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
			 try {
	
				 String username = request.getParameter("username");
				 boolean status = registerdao.deluser(username);
				
				}		
				catch (Exception e) {
					e.printStackTrace();
				}
			}
		
	
}
