package com.raghu.springsecurity.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.raghu.springsecurity.DAO.LikeDAO;
import com.raghu.springsecurity.DAO.RegisterDAO;
//import com.raghu.springsecurity.entity.Likes;
import com.raghu.springsecurity.service.LikeService;


@Controller
public class AddLikeController  extends HttpServlet {

	@Autowired 
	private LikeService likeservice;
	
	
	@Autowired 
	private LikeDAO likedao;

	private AnnotationConfigApplicationContext ctx;

	
	 @RequestMapping("/home/blog/like")
	 protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		 try {

			 String username = request.getParameter("username");
			
			 String blogid = request.getParameter("blog_id");
			 int blog_id=Integer.parseInt(blogid);  
			 
			
			  likedao.addlike(blog_id, username);

			}		
			catch (Exception e) {
				e.printStackTrace();
			}
		}

	
}
