package com.raghu.springsecurity.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.raghu.springsecurity.DAO.CommentDAO;
import com.raghu.springsecurity.DAO.FriendDAO;
import com.raghu.springsecurity.DAO.LikeDAO;

@Controller
public class AddCommentController {
	
	@Autowired
	private CommentDAO commentdao;
	
	 @RequestMapping("/home/blog/comment")
	 protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		 try {

			 String username = request.getParameter("username");
				
			 String blogid = request.getParameter("blog_id");
			
			 int blog_id=Integer.parseInt(blogid);
			 
			 String comment = request.getParameter("comment");
			 
			
			 commentdao.addcomment(blog_id, username,comment);


			
			}		
			catch (Exception e) {
				e.printStackTrace();
			}
		}
}
