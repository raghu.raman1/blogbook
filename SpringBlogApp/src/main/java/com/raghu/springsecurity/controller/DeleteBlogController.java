package com.raghu.springsecurity.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.raghu.springsecurity.DAO.BlogDAO;

@Controller
public class DeleteBlogController  extends HttpServlet  {

	@Autowired
	private BlogDAO blogdao;
	
	 @RequestMapping("/home/deleteblog")
	 protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		 try {
	
			 String blogid = request.getParameter("blog_id");
			 int blog_id=Integer.parseInt(blogid);  
			 
		     boolean status = blogdao.deleteblog(blog_id);
		
			
			}		
			catch (Exception e) {
				e.printStackTrace();
			}
		}
	
	

	 
}
