package com.raghu.springsecurity.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.raghu.springsecurity.DAO.BlogDAO;

@Controller
public class BlogController  extends HttpServlet  {

	@Autowired
	private BlogDAO blogdao;
	
	 @RequestMapping("/home/addblog")
	 protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		 try {
			 String blog = request.getParameter("blog");
             String username = request.getParameter("username");


		      boolean status = blogdao.addblog(blog , username);
		
			
			}		
			catch (Exception e) {
				e.printStackTrace();
			}
		}
	
	

	 
}
