<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <security:authentication var="principal" property="principal" />
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Add blog</title>
</head>
<body>
<form action="${request.contextPath}/blogApp/home/addblog" method="post">

  
      <div class="form-group">
       <label for="blog">What's On your Mind ?</label> <input type="text"
        class="form-control" id="blog" placeholder="Share your thoughts by typing here..."
        name="username" required>
      </div>
    
     <div class="form-group">
       <input type="hidden"
        class="form-control" id="username" 
        name="password" value=${principal.username}>
      </div>


      <button type="submit" class="btn btn-primary">Post Blog</button>

     </form>



</body>
</html>